import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

void main() => runApp(MaterialApp(
  home: my_Home_Screen(),
));


class my_Home_Screen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mera pehela ape'),
        centerTitle: true,
        backgroundColor: Colors.deepOrange[600],
      ),

      body: Row(
        children: [
          Expanded(
            flex: 3,
            child: Container(
              padding: EdgeInsets.all(30.00),
              color: Colors.amber,
              child: Text(" 1"),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              padding: EdgeInsets.all(30.00),
              color: Colors.cyan,
              child: Text(" 2"),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.all(30.00),
              color: Colors.purpleAccent,
              child: Text(" 3"),
            ),
          )
        ],
      ),

      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(height: 50.0),
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: (){print("hey you");},
        tooltip: 'Increment Counter',
        child: Text("click"),
        backgroundColor: Colors.deepOrange[600],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
    

